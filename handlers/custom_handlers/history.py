from telebot.types import Message
import logging
from loader import bot



# @bot.message_handler(commands=["history"])
# def bot_history(message: Message):
#     bot.reply_to(
#         message, "Узнать историю запросов"
#     )
# @bot.message_handler(regexp='g')
# def log_chat(message: Message):
#     print(message.text)
#     text = message.text
#     print(text)
#     if message.content_type == "text":
#         if text != '/':
#             print(text)



class BotLogger:
    def __init__(self, log_file):
        self.logger = logging.getLogger("bot_logger")
        self.logger.setLevel(logging.DEBUG)
        formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
        file_handler = logging.FileHandler(log_file)
        file_handler.setFormatter(formatter)
        self.logger.addHandler(file_handler)

    def log(self, level, message):
        if level == 'info':
            self.logger.info(message)
        elif level == 'error':
            self.logger.error(message)
        elif level == 'debug':
            self.logger.debug(message)

    def log_info(self, message):
        self.log('info', message)


logger = BotLogger('bot.log')


# Далее в коде бота
@bot.message_handler(func=lambda message: True)
def handle_message(message):
    # Обработка сообщений от пользователей
    user_message = message.text
    logger.log_info(f"Получено сообщение от пользователя: {user_message}")

    # Отправка ответа
    bot.reply_to(message, "Ваш запрос получен и обработан.")
