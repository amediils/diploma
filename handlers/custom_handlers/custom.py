from telebot.types import Message
from loader import bot
import requests
import json
from telebot import types

def choice_procent(message):
    mesg = bot.send_message(message.chat.id,'Выше какого процента должнен быть рейтинг аниме?')
    bot.register_next_step_handler(mesg,averageScore_greater)


def averageScore_greater(message):
    global averageScore_greater
    averageScore_greater = int(message.text)

@bot.message_handler(commands=["custom"])
def bot_custom(message: Message):
    markup = types.InlineKeyboardMarkup(row_width=2)
    btn_anime = types.InlineKeyboardButton('Аниме', callback_data='anime_custom')
    btn_manga = types.InlineKeyboardButton('Манга', callback_data='manga_custom')
    markup.add(btn_anime, btn_manga)
    bot.send_message(message.chat.id, 'Искать наиболее популярное аниме или мангу?', reply_markup=markup)
    #print(message)


@bot.callback_query_handler(func=lambda call: call.data == "anime_custom" or call.data == "manga_custom")
def callback_custom(call):
    #print(call)
    #print(call.message)
    global type_of_content
    if call.message:
        if call.data == 'anime_custom':
            type_of_content = "ANIME"
        elif call.data == 'manga_custom':
            type_of_content = "MANGA"
    choice_procent(call.message)



    markup = types.InlineKeyboardMarkup(row_width=2)
    btn_top1 = types.InlineKeyboardButton('1', callback_data='1')
    btn_top3 = types.InlineKeyboardButton('3', callback_data='3')
    btn_top5 = types.InlineKeyboardButton('5', callback_data='5')
    btn_top10 = types.InlineKeyboardButton('10', callback_data='10')

    markup.add(btn_top1, btn_top3, btn_top5, btn_top10)
    bot.send_message(call.message.chat.id, 'Сколько позиций должно быть в топе?', reply_markup=markup)
    #choice_procent(call.message)

@bot.callback_query_handler(func=lambda call: call.data == "3" or call.data == "5" \
                                              or call.data == "10" or call.data == "1")
def callcount_custom(call):
    global count_titles
    if call.data == "3":
        count_titles = 3

    elif call.data == "5":
        count_titles = 5

    elif call.data == "10":
        count_titles = 10

    elif call.data == "1":
        count_titles = 1



    # averageScore_greater = 65
    # bot.send_message(call.message.chat.id, 'Выше какого процента должнен быть рейтинг аниме?')
    # bot.register_next_step_handler(message, )



    query = '''
    query ($page: Int, $perPage: Int, $type: MediaType,  $averageScore_greater: Int){ 
    Page (page: $page, perPage: $perPage) {
        pageInfo {
            total
            #currentPage
            #lastPage
            #hasNextPage
           # perPage
        }
        media (type: $type, averageScore_greater: $averageScore_greater) {
            startDate {year}

            title {
                english
                romaji
            }
        }
    }
}
    '''

    variables = {
        'type': type_of_content,
        'page': 1,
        'perPage': count_titles,
        "averageScore_greater": averageScore_greater
    }


    url = 'https://graphql.anilist.co'
    #print(averageScore_greater)
    response = requests.post(url, json={'query': query, 'variables': variables})
    text = response.text
    parsed = json.loads(text)
    print(parsed)
    if type_of_content == "MANGA":
        bot.send_message(call.message.chat.id, f"Манга с рейтингом выше {averageScore_greater}:")
        for num_title in range(count_titles):
            message_top = f"Название: " + str(
                parsed["data"]["Page"]['media'][num_title]['title']["romaji"]) + \
                          "\nГод выхода: " + str(parsed["data"]["Page"]['media'][num_title]["startDate"]["year"])
            bot.send_message(call.message.chat.id, message_top)

    elif type_of_content == "ANIME":
        bot.send_message(call.message.chat.id, f"Аниме с рейтингом выше {averageScore_greater}:")
        for num_title in range(count_titles):
            message_top = f"Название: " + str(
                parsed["data"]["Page"]['media'][num_title]['title']["romaji"]) + \
                          "\nГод выхода: " + str(parsed["data"]["Page"]['media'][num_title]["startDate"]["year"])
            bot.send_message(call.message.chat.id, message_top)
