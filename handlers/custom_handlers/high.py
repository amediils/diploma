from telebot.types import Message
from loader import bot
import requests
import json
from telebot import types



@bot.message_handler(commands=["high"])
def bot_high(message: Message):

    markup = types.InlineKeyboardMarkup(row_width=2)
    btn_anime = types.InlineKeyboardButton('Аниме', callback_data='anime')
    btn_manga = types.InlineKeyboardButton('Манга', callback_data='manga')
    markup.add(btn_anime, btn_manga)
    bot.send_message(message.chat.id,  'Искать наиболее популярное аниме или мангу?', reply_markup=markup)


@bot.callback_query_handler(func=lambda call:call.data == "anime" or call.data == "manga")
def callback(call):
    global type_of_content
    if call.message:
        if call.data == 'anime':
            type_of_content = "ANIME"
        elif call.data == 'manga':
            type_of_content = "MANGA"


    markup = types.InlineKeyboardMarkup(row_width=2)
    btn_top3 = types.InlineKeyboardButton('Топ 3', callback_data='top3')
    btn_top5 = types.InlineKeyboardButton('Топ 5', callback_data='top5')
    btn_top10 = types.InlineKeyboardButton('Топ 10', callback_data='top10')
    btn_top1 = types.InlineKeyboardButton('Топ 1', callback_data='top1')
    markup.add(btn_top3, btn_top5, btn_top10, btn_top1)
    bot.send_message(call.message.chat.id,  'Сколько позиций должно быть в топе?', reply_markup=markup)

@bot.callback_query_handler(func=lambda call:call.data == "top3" or call.data == "top5" \
                            or call.data == "top10" or call.data == "top1")
def callcount(call):
    global count_titles
    if call.data == "top3":
        count_titles = 3

    elif call.data == "top5":
        count_titles = 5

    elif call.data == "top10":
        count_titles = 10

    elif call.data == "top1":
        count_titles = 1



    query = '''
    query ($page: Int, $perPage: Int, $type: MediaType){ # Define which variables will be used in the query (id)
    Page (page: $page, perPage: $perPage) {
        pageInfo {
            total
            #currentPage
            #lastPage
            #hasNextPage
           # perPage
        }
        media (type: $type, sort: POPULARITY_DESC) {
            startDate {year}

            title {
                english
            }
        }
    }
}
    '''

    variables = {
        'type': type_of_content,
        'page': 1,
        'perPage': count_titles
    }


    url = 'https://graphql.anilist.co'
    response = requests.post(url, json={'query': query, 'variables': variables})
    text = response.text
    parsed = json.loads(text)
    if type_of_content == "MANGA":
        bot.send_message(call.message.chat.id, f"Топ {count_titles} манги")
        for num_title in range(count_titles):
            message_top = f"Самая популярная манга {num_title+1} место \nНазвание: " + str(parsed["data"]["Page"]['media'][num_title]['title']["english"]) + \
                "\nГод выхода: " + str(parsed["data"]["Page"]['media'][num_title]["startDate"]["year"])
            bot.send_message(call.message.chat.id, message_top)

    elif type_of_content == "ANIME":
        bot.send_message(call.message.chat.id, f"Топ {count_titles} аниме")
        for num_title in range(count_titles):
            message_top = f"Самое популярное аниме {num_title+1} место \nНазвание: " + str(parsed["data"]["Page"]['media'][num_title]['title']["english"]) + \
                "\nГод выхода: " + str(parsed["data"]["Page"]['media'][num_title]["startDate"]["year"])
            bot.send_message(call.message.chat.id, message_top)
